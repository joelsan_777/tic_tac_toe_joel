<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Partidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_partida')->nullable();
            $table->string('nombre_creador')->nullable();
            $table->string('nombre_invitado')->nullable();
            $table->string('movimientos')->nullable();
            $table->string('turno_actual')->nullable();
            $table->string('estado_partida')->nullable();
            $table->string('ganador')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partidas');
    }
}
