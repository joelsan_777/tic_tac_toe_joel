<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partidas extends Model
{
    protected $fillable = [
    	'codigo_partida',
    	'nombre_creador',
    	'nombre_invitado',
    	'movimientos',
    	'turno_actual',
    	'estado_partida',
    	'ganador'
    ];
}
