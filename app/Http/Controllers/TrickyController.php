<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partidas;

class TrickyController extends Controller
{
    public function crear_sala(Request $request){
    	$existente = Partidas::where('codigo_partida',$request->codigo_n)->first();
    	if (empty($existente)) {
    		$nueva_partida = new Partidas();
	    	$nueva_partida->codigo_partida = $request->codigo_n;
	    	$nueva_partida->nombre_creador = 'Jugador 1';
	    	$nueva_partida->movimientos = '{"p1":"","p2":"","p3":"","p4":"","p5":"","p6":"","p7":"","p8":"","p9":""}';
	    	$nueva_partida->turno_actual = 'creador';
	    	$nueva_partida->estado_partida = 'iniciado';
	    	$nueva_partida->save();
	    	return view('modulos.creador', array('nueva_partida'=>$nueva_partida));	
    	}else {
    		return view('modulos.creador', array('nueva_partida'=>$existente));	
    	}
    }

    public function actualizar_nombre(Request $request){
    	$partida = Partidas::find($request->id);
    	if ($request->jugador == 'creador') {
    		$partida->nombre_creador = $request->nombre;	
    	}else{
    		$partida->nombre_invitado = $request->nombre;	
    	}
    	$partida->save();
    }

    public function actualizar_movimientos(Request $request){
    	$partida = Partidas::find($request->id);
    	$partida->movimientos = $request->mov;
    	$partida->turno_actual = $request->turno;
    	$partida->save();
    }

    public function unirse_sala(Request $request){
    	$buscar_partida = Partidas::where('codigo_partida', $request->codigo)->first();
    	if (!empty($buscar_partida)) {
    		$partida = Partidas::find($buscar_partida->id);
    		if (empty($partida->nombre_invitado)) {
    			$partida->nombre_invitado = 'Jugador 2';	
    		}
    		$partida->estado_partida = 'jugando';
    		$partida->save();
    		return view('modulos.invitado', array('partida'=>$partida));
    	}else{
    		return 'no hay partida';
    	}
    }

    public function obtener_actual($id){
    	return Partidas::find($id);
    }

    public function ganar_partida($id, $jugador){
    	$partida = Partidas::find($id);
    	$partida->estado_partida = 'finalizada';
    	if ($jugador == 'creador') {
    		$partida->ganador = $partida->nombre_creador;	
    	}else if ($jugador == 'invitado') {
    		$partida->ganador = $partida->nombre_invitado;
    	}else{
    		$partida->ganador = 'ninguno';
    	}
    	$partida->save();
    	return $partida;
    }

}
