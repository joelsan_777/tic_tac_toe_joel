<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('modulos.inicio');
});
Route::get('new_game','TrickyController@crear_sala')->name('nueva_partida');
Route::post('update_name','TrickyController@actualizar_nombre');
Route::post('update_movements','TrickyController@actualizar_movimientos');
Route::get('join_game','TrickyController@unirse_sala')->name('unirse_partida');
Route::get('get_current/{id}','TrickyController@obtener_actual');
Route::get('win_duel/{id}/{jugador}','TrickyController@ganar_partida');
