@extends('layouts.app')

@section('content')

    <div class="uk-container uk-container-large">
        <div class="uk-margin">
            <div class="uk-grid">
                <div class="uk-width-1-3"></div>
                <div class="uk-width-1-3">
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-header">
                            <h1 class="uk-heading-line uk-text-center" style="color: #333333;">
                                <span uk-icon="icon: github; ratio: 3.5"></span>
                            </h1>
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title uk-text-center">¡TIC-TAC-TOE!</h3>
                            <form class="uk-grid-small" action="{{ route('nueva_partida') }}" method="GET" uk-grid>
                            	<div class="uk-width-1-1">
                            		<input type="hidden" value="{{rand(100000,999999)}}" name="codigo_n" id="codigo_n">
                            		<button type="submit" class="uk-button uk-button-primary uk-width-1-1">CREAR PARTIDA</button>
                            	</div>
                            </form>
                            <hr class="uk-divider-icon">
                            <form class="uk-grid-small" action="{{ route('unirse_partida') }}" method="GET" uk-grid>
							    <div class="uk-width-1-2">
							        <div class="uk-margin">
								        <div class="uk-inline">
								            <span class="uk-form-icon" uk-icon="icon: hashtag"></span>
								            <input class="uk-input" type="number" name="codigo" id="codigo" required>
								        </div>
								    </div>
							    </div>
							    <div class="uk-width-1-2">
							        <button type="submit" class="uk-button uk-button-secondary uk-width-1-1">UNIRSE</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3"></div>
            </div>
        </div>
    </div>
    
@endsection